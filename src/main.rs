use rusqlite::backup::{Backup, Progress};
use rusqlite::{Connection, OpenFlags};
use std::env;
use std::fs::copy;
use std::path::Path;
use std::time::Duration;
use tempfile::NamedTempFile;

fn progress(progress: Progress) {
    println!("{:?}", progress)
}

fn main() {
    let args = env::args().nth(1).expect("Requires 1 argument");

    let src_file = Path::new(Path::new(&args));
    let tmpfile = NamedTempFile::new().unwrap();
    let tmpfile_path = tmpfile.path();
    println!("{}", src_file.metadata().unwrap().len() / 1000);
    println!("{}", tmpfile_path.display());

    let src_db = Connection::open_with_flags(
        src_file,
        OpenFlags::SQLITE_OPEN_READ_ONLY
            | OpenFlags::SQLITE_OPEN_NO_MUTEX
            | OpenFlags::SQLITE_OPEN_PRIVATE_CACHE,
    )
    .unwrap();

    let mut dest_db = Connection::open(tmpfile_path).unwrap();
    {
        let backup = Backup::new(&src_db, &mut dest_db).unwrap();
        backup
            .run_to_completion(100, Duration::from_millis(100), Some(progress))
            .unwrap();
    }

    src_db.close().unwrap();
    dest_db.close().unwrap();

    copy(tmpfile_path, Path::new("./backup.db")).unwrap();
}
